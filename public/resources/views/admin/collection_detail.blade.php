@extends('admin.layout')
@section('header') Nhóm sản phẩm
@endsection
@section('content')
    <style media="screen">
        input[type=radio] {
            margin-right: 10px;
            margin-left: 70px;
        }
    </style>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="box box-info">
                <div class="form-horizontal form-product">
                    <div class="box-title clearfix">
                        <div class="pull-left title-tab">{{$collection->title}} | Lượt xem: {{$collection->view}}
                            <div class="action_result">
                                <span style="color: red;"></span>
                            </div>
                        </div>
                        <div class="pull-right title-tab">
                            <a href="http://localhost:9000/collection?id={{$collection->id}}" target="_blank"
                               class="next-preview">Xem trên website<i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Tên nhóm<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12">
                                        <input name="name" value="{{$collection->title}}" placeholder="Tên nhóm"
                                               class="form-control title text-overflow-title">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Mô tả<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12">
                                        <textarea name="description" rows="8" cols="80" placeholder="Mô tả"
                                                  class="form-control title text-overflow-title">{{$collection->description}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Nội dung<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12">
                                        <textarea id="collection_content" name="content" rows="8" cols="80"
                                                  placeholder="Mô tả"
                                                  class="tinymce form-control title text-overflow-title">{{$collection->content}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                          <div class="tab-content">
                            <div class="form-group">
                              <div class="col-lg-2 col-md-12 control-label">Chọn sản phẩm</div>
                              <div class="col-lg-10 col-md-12">
                                <select data-ids="{{$collection->product_ids}}" multiple="multiple" class="multiple-select settings" name="product_ids">
                                    @foreach (Helper::getAllProduct() as $product)
                                        <option value="{{$product->id}}">{{$product->title}}</option>
                                    @endforeach
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Hình đại diện<strong class="required">*</strong>
                                    </div>
                                    <div class="col-lg-4 col-md-12">
                                        <div class="image-preview" data-key="{{$collection->id}}" data-type="collection"
                                             name="image">
                                            <label for="image-upload" class="image-label">Chọn hình</label>
                                            <input type="file" name="image" class="image-upload"/>
                                        </div>
                                    </div>
                                    <input type="hidden" name="id" value="{{$collection->id}}">
                                    <div class="col-lg-6 col-md-12">Trạng thái:<br>
                                        <input type="radio" name="status" value="active"
                                               @if ($collection->status == 'active')
                                               checked=true
                                                @endif
                                        ><span class="label label-success">Sẵn sàng</span><br>
                                        <input type="radio" name="status" value="deleted"
                                               @if ($collection->status == 'deleted')
                                               checked=true
                                                @endif
                                        ><span class="label label-danger">Bị xóa</span><br>
                                        <input type="radio" name="status" value="hiden"
                                               @if ($collection->status == 'hiden')
                                               checked=true
                                                @endif
                                        ><span class="label label-warning">Bị ẩn</span><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-4 col-md-4 pull-right">
                                        <input type="button" id="collection-update" value="Lưu"
                                               class="form-control title text-overflow-title btn btn-primary">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        tinymce.init({
            selector: 'textarea.tinymce',
            plugins: 'codesample'
        });
        $(document).ready(function () {
            $.uploadPreview({
                input_field: ".image-upload", // Default: .image-upload
                preview_box: ".image-preview", // Default: .image-preview
                label_field: ".image-label", // Default: .image-label
                label_default: "Chọn hình", // Default: Choose File
                label_selected: "", // Default: Change File
                no_label: true
            });
            initImage();
            $(function() {
              $('.multiple-select').multipleSelect({
                width: '100%'
              });
            });
            $(document).ready(function () {
                //init multile selects
                $.each($('select.multiple-select'), function (i, e) {
                  var dataIds = $(e).data('ids');
                  var ids = [];
                  $.each(dataIds, function(i, e) {
                    ids.push(e.id);
                  })
                  $(e).multipleSelect('setSelects', ids);
                })
            })
            $('#collection-update').on('click', function () {
                try {
                    var name = $('input[name=name]').val();
                    var image = "";
                    if (typeof($('input[name=image]').prop('files')[0]) === 'undefined') {
                        var image_token = $('input[name=image]').parent().css('background-image').split('/');
                        var image = image_token[image_token.length - 1].replace('")', '');
                    } else {
                        image = $('input[name=image]').prop('files')[0].name;
                    }
                    var description = $('textarea[name=description]').val();
                    var content = tinymce.get('collection_content').getContent();
                    var status = $('input[name=status]:checked').val();
                    var product_ids = $('select[name="product_ids"]').val();
                    var id = $('input[name=id]').val();
                } catch (error) {
                    $('.action_result span').text('Hãy nhập đầy đủ thông tin');
                    window.scrollTo(document.body.scrollWidth, 0);
                    return;
                }
                if (name == "" || description == "" || content == "" || status == "") {
                    $('.action_result span').text('Hãy nhập đầy đủ thông tin');
                    window.scrollTo(document.body.scrollWidth, 0);
                    return;
                }
                var file = $('input[name=image]').prop('files')[0];
                if (typeof(file) !== 'undefined') {
                    var formdata = new FormData();
                    formdata.append('image', file);
                    var params1 = {
                        url: '/admin/uploadImage',
                        type: 'POST',
                        data: formdata,
                        processData: false,
                        contentType: false,
                        success: function (result1) {
                            var image = $.parseJSON(result1).image_name;
                            var params = {
                                url: '/admin/updateCollection',
                                type: 'POST',
                                data: {
                                    name: name,
                                    image: image,
                                    description: description,
                                    content: content,
                                    status: status,
                                    product_ids: product_ids,
                                    id: id
                                },
                                success: function (result) {
                                    alert('Lưu thành công');
                                    window.location.href = '/admin/collection_all';
                                }
                            }
                            $.ajax(params);
                        }
                    }
                    $.ajax(params1);
                } else {
                    var params = {
                        url: '/admin/updateCollection',
                        type: 'POST',
                        data: {
                            name: name,
                            image: image,
                            description: description,
                            content: content,
                            status: status,
                            product_ids: product_ids,
                            id: id
                        },
                        success: function (result) {
                            alert('Lưu thành công');
                            window.location.href = '/admin/collection_all';
                        }
                    }
                    $.ajax(params);
                }
            })
        })
    </script>
@endsection
