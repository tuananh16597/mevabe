function initImage() {
  $.each($('.image-preview'), function(i, e) {
    var params = {
      type: 'POST',
      url: '/admin/getImage',
      data: {
        key: $(e).data('key'),
        type: $(e).data('type')
      },
      success: function(result) {
        result = $.parseJSON(result);
        if (result.code == 0 && result.data != "") {
          $(e).css("background", "url('/uploads/" + result.data + "')");
          $(e).css("background-size", "cover");
          $(e).css("background-position", "center center");
        }
      }
    }
    $.ajax(params);
  })
}
function money(num) {
  if (num) num = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
  return num;
}
