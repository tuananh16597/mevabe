@extends('admin.layout')
@section('header') Thiết lập chung
@endsection
@section('content') {{-- Document Thêm một trường mới vào settings: Nhớ đổi name của input, select Copy cấu trúc 1 block ở dưới
<div class="box-body">
   <div class="tab-content">
      <div class="form-group">
         <div class="col-lg-4 col-md-12 control-label">Tên shop<strong class="required">*</strong></div>
         <div class="col-lg-8 col-md-12">
            <input name="shop-name" placeholder="Tên shop" class="settings form-control title text-overflow-title" value="{{$settings->get('shop-name')}}">
         </div>
      </div>
   </div>
</div>
Tên của input là name của meta Value của input là value của meta --------------------> Cú pháp lấy biển từ settings bằng key {{$settings->get('key-can-lay')}} --------------------> Cú pháp multiple select
<div class="box-body">
   <div class="tab-content">
      <div class="form-group">
         <div class="col-lg-4 col-md-12 control-label">Sản phẩm nổi bật<strong class="required">*</strong></div>
         <div class="col-lg-8 col-md-12">
            <select multiple="multiple" class="multiple-select settings" name="san-pham-noi-bat">
               @foreach (Helper::getAllProduct() as $product)
               <option value="{{$product->id}}">{{$product->title}}</option>
               @endforeach
            </select>
         </div>
      </div>
   </div>
</div>
Lấy type nào thì get hết type đó về, đặt tên select là tên của meta, jquery sẽ tự động check --}}
<div class="row">
   <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="box box-info">
         <div class="form-horizontal form-product">
            <div class="box-title clearfix">
               <div class="pull-left title-tab">Thông tin liên hệ
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Tên shop<strong class="required">*</strong>
                     </div>
                     <div class="col-lg-8 col-md-12"><input name="shop-name" placeholder="Tên shop"
                        class="settings form-control title text-overflow-title"
                        value="{{$settings->get('shop-name')}}"></div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Phone<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12"><input name="phone" placeholder="Số điện thoại"
                        class="settings form-control title text-overflow-title"
                        value="{{$settings->get('phone')}}"></div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Youtube<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <input name="youtube" placeholder="https://www.youtube.com/"
                           class="settings form-control title text-overflow-title"
                           value="{{$settings->get('youtube')}}">
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Twitter<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <input name="twitter" placeholder="https://twitter.com"
                           class="settings form-control title text-overflow-title"
                           value="{{$settings->get('twitter')}}">
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Facebook<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <input name="facebook" placeholder="https://facebook.com"
                           class="settings form-control title text-overflow-title"
                           value="{{$settings->get('facebook')}}">
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Rss<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <input name="rss" placeholder="https://www.rss.com/"
                           class="settings form-control title text-overflow-title"
                           value="{{$settings->get('rss')}}">
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Khẩu hiệu<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <input name="slogan"
                           placeholder="Đồ chơi trẻ em giúp cho các bé vừa được vui chơi ..."
                           class="settings form-control title text-overflow-title"
                           value="{{$settings->get('slogan')}}">
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                      <div class="col-lg-4 col-md-12 control-label">Địa chỉ<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <input name="address" placeholder="123 Đường ABC, Quận ABC, Tp: HCM"
                           class="settings form-control title text-overflow-title"
                           value="{{$settings->get('address')}}">
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                      <div class="col-lg-4 col-md-12 control-label">Giờ làm việc<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <input name="working_time" placeholder="Thứ 2 - 6: 7h - 21h"
                           class="settings form-control title text-overflow-title"
                           value="{{$settings->get('working_time')}}">
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                      <div class="col-lg-4 col-md-12 control-label">Bản quyền<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <input name="powerby" placeholder="Children Toys"
                           class="settings form-control title text-overflow-title"
                           value="{{$settings->get('powerby')}}">
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                      <div class="col-lg-4 col-md-12 control-label">Thiết kế<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <input name="copyright" placeholder="Chili"
                           class="settings form-control title text-overflow-title"
                           value="{{$settings->get('copyright')}}">
                     </div>
                  </div>
               </div>
            </div>
            {{--
            <div class="box-body">
               --}}
               {{--
               <div class="tab-content">
                  --}}
                  {{--
                  <div class="form-group">
                     --}}
                     {{--
                     <div class="col-lg-4 col-md-4 pull-right"><input name="submit" type="button" value="Áp dụng"--}}
                        {{--class="form-control title text-overflow-title">--}}
                        {{--
                     </div>
                     --}}
                     {{--
                  </div>
                  --}}
                  {{--
               </div>
               --}}
               {{--
            </div>
            --}}
         </div>
      </div>
      <div class="box">
         <div class="form-horizontal">
            <div class="box-title clearfix">
               <div class="pull-left title-tab">Dịch vụ mua hàng
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                      <div class="col-lg-4 col-md-12 control-label">Dịch vụ 1<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <input name="service_1" placeholder="Miễn phí vận chuyển"
                           class="settings form-control title text-overflow-title"
                           value="{{$settings->get('service_1')}}">
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Dịch vụ 2<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <input name="service_2" placeholder="Miễn phí vận chuyển"
                           class="settings form-control title text-overflow-title"
                           value="{{$settings->get('service_2')}}">
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Dịch vụ 3<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <input name="service_3" placeholder="Miễn phí vận chuyển"
                           class="settings form-control title text-overflow-title"
                           value="{{$settings->get('service_3')}}">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="box">
         <div class="form-horizontal">
            <div class="box-title clearfix">
               <div class="pull-left title-tab">Sản phẩm trưng bày
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Nhóm sản phẩm trưng bày (Hiện tối đa 3 nhóm)<strong
                        class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <select multiple="multiple" class="multiple-select settings" name="desktop-colllections">
                           @foreach (Helper::getAllCollection() as $collection)
                           <option value="{{$collection->id}}">{{$collection->title}}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Sản phẩm nổi bật (Hiện tối đa 4 sản phẩm)<strong
                        class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <select multiple=“multiple” class="multiple-select settings" name="hot-products">
                           @foreach (Helper::getAllProduct() as $product)
                           <option value="{{$product->id}}">{{$product->title}}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Nhóm đặc biệt (Hiện ở trang chủ)<strong
                        class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <select multiple="multiple" class="multiple-select settings" name="special-collection">
                           @foreach (Helper::getAllCollection() as $collection)
                           <option value="{{$collection->id}}">{{$collection->title}}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                    <div class="col-lg-4 col-md-12 control-label">Sản phẩm mới nhất<strong
                       class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <select multiple="multiple" class="multiple-select settings"
                           name="new-products">
                           @foreach (Helper::getAllProduct() as $product)
                           <option value="{{$product->id}}">{{$product->title}}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                    <div class="col-lg-4 col-md-12 control-label">Sản phẩm bán chạy<strong
                       class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <select multiple="multiple" class="multiple-select settings"
                           name="sale-products">
                           @foreach (Helper::getAllProduct() as $product)
                           <option value="{{$product->id}}">{{$product->title}}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                    <div class="col-lg-4 col-md-12 control-label">Sản phẩm khuyến mãi<strong
                       class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <select multiple="multiple" class="multiple-select settings"
                           name="promotion-products">
                           @foreach (Helper::getAllProduct() as $product)
                           <option value="{{$product->id}}">{{$product->title}}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="box">
         <div class="form-horizontal">
            <div class="box-title clearfix">
               <div class="pull-left title-tab">Hình ảnh liên quan
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Logo<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <div class="image-preview" data-key="logo" name="logo">
                           <label for="image-upload" class="image-label">Chọn hình</label>
                           <input type="file" name="logo" class="image-upload"/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Favicon<strong class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <div class="image-preview" data-key="favicon" name="favicon">
                           <label for="image-upload" class="image-label">Chọn hình</label>
                           <input type="file" name="favicon" class="image-upload"/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="box">
         <div class="form-horizontal">
            <div class="box-title clearfix">
               <div class="pull-left title-tab">Màn chiếu trang chủ
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Slide 1<strong class="required">*</strong>
                     </div>
                     <div class="col-lg-8 col-md-12">
                        <div class="image-preview" data-key="slide_1" name="slide_1">
                           <label for="image-upload" class="image-label">Chọn hình</label>
                           <input type="file" name="slide_1" class="image-upload"/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Slide 2<strong class="required">*</strong>
                     </div>
                     <div class="col-lg-8 col-md-12">
                        <div class="image-preview" data-key="slide_2" name="slide_2">
                           <label for="image-upload" class="image-label">Chọn hình</label>
                           <input type="file" name="slide_2" class="image-upload"/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Slide 3<strong class="required">*</strong>
                     </div>
                     <div class="col-lg-8 col-md-12">
                        <div class="image-preview" data-key="slide_3" name="slide_3">
                           <label for="image-upload" class="image-label">Chọn hình</label>
                           <input type="file" name="slide_3" class="image-upload"/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Slide 4<strong class="required">*</strong>
                     </div>
                     <div class="col-lg-8 col-md-12">
                        <div class="image-preview" data-key="slide_4" name="slide_4">
                           <label for="image-upload" class="image-label">Chọn hình</label>
                           <input type="file" name="slide_4" class="image-upload"/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="box">
         <div class="form-horizontal">
            <div class="box-title clearfix">
               <div class="pull-left title-tab">Thương hiệu nổi bật
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Thương hiệu 1<strong
                        class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <div class="image-preview" data-key="brand_1" name="brand_1">
                           <label for="image-upload" class="image-label">Chọn hình</label>
                           <input type="file" name="brand_1" class="image-upload"/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Thương hiệu 2<strong
                        class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <div class="image-preview" data-key="brand_2" name="brand_2">
                           <label for="image-upload" class="image-label">Chọn hình</label>
                           <input type="file" name="brand_2" class="image-upload"/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Thương hiệu 3<strong
                        class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <div class="image-preview" data-key="brand_3" name="brand_3">
                           <label for="image-upload" class="image-label">Chọn hình</label>
                           <input type="file" name="brand_3" class="image-upload"/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body">
               <div class="tab-content">
                  <div class="form-group">
                     <div class="col-lg-4 col-md-12 control-label">Thương hiệu 4<strong
                        class="required">*</strong></div>
                     <div class="col-lg-8 col-md-12">
                        <div class="image-preview" data-key="brand_4" name="brand_4">
                           <label for="image-upload" class="image-label">Chọn hình</label>
                           <input type="file" name="brand_4" class="image-upload"/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <button class="btn btn-submit" id="btn-admit">
      <span class="icon-loading"><i class="fa fa-floppy-o"></i></span>
      Áp dụng
      </button>
      <style>
         .btn-submit {
         color: #2b3643;
         border-color: #2b3643 !important;
         }
         .btn-submit:hover {
         color: #fff !important;
         background-color: #2b3643 !important;
         }
      </style>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function() {
       $.uploadPreview({
           input_field: ".image-upload", // Default: .image-upload
           preview_box: ".image-preview", // Default: .image-preview
           label_field: ".image-label", // Default: .image-label
           label_default: "Chọn hình", // Default: Choose File
           label_selected: "", // Default: Change File
           no_label: true
       });
       $.each($('.image-preview'), function(i, e) {
           var params = {
               type: 'POST',
               url: '/admin/getMeta',
               data: {
                   name: $(e).attr('name')
               },
               success: function(result) {
                   if (result.code == 0 && result.value != "") {
                       $(e).css("background-image", "url(/uploads/" + result.value + ")");
                       $(e).css("background-size", "cover");
                       $(e).css("background-position", "center center");
                   }
               }
           }
           $.ajax(params);
       })
   });
</script>
<script type="text/javascript">
   $(function () {
       $('.multiple-select').multipleSelect({
           width: '100%'
       });
   });
   $(document).ready(function () {
       //init multile selects
       $.each($('select.multiple-select'), function (i, e) {
           var params = {
               type: 'POST',
               url: '/admin/getMeta',
               data: {
                   name: $(e).attr('name')
               },
               success: function (result) {
                   if (result.code == 0) {
                       var ids = result.value.split(',');
                       $(e).multipleSelect('setSelects', ids);
                   }
               }
           }
           $.ajax(params);
       })
   })
   $('#btn-admit').on('click', function (e) {
       e.preventDefault();
       $('.btn-submit i').hide();
       $('.btn-submit span').append('<i class="fa fa-circle-o-notch fa-spin"></i>');
       saveAllInput(saveAllImage(function () {
           setTimeout(function () {
               window.location.href = '/admin/settings';
           }, 5000);
       }));
       // $.when(saveAllInput(), saveAllImage()).done(function() {
       //   console.log('Done!');
       //   // window.location.href='/admin/settings';
       // })

   })

   function saveAllInput(_callback) {
       $.each($('.settings'), function (i, e) {
           if ($(e).is('div')) {
               return true;
           }
           var value = $(e).val();
           if ($(e).is('select')) {
               value = $(e).val().join(',');
           }
           var params = {
               type: 'POST',
               url: '/admin/updateMeta',
               data: {
                   name: $(e).attr('name'),
                   value: value
               },
               success: function (result) {
               }
           }
           $.ajax(params);
       })

       if (_callback) _callback();
   }

   function saveAllImage(_callback) {
       $.each($('input.image-upload'), function(i, e) {
           var file = $(e).prop('files')[0];
           if (typeof(file) === 'undefined') {
               if ($(e).parent().css('background-image') != 'none') {
                   var image_token = $(e).parent().css('background-image').split('/');
                   var image_name = image_token[image_token.length-1].replace('")', '');
                   var params = {
                       type: 'POST',
                       url: '/admin/updateMeta',
                       data: {
                           name: $(e).parent().attr('name'),
                           value: image_name
                       },
                       success: function(result) {}
                   }
                   $.ajax(params);
               }
           } else {
               var formdata = new FormData();
               formdata.append('image', file);
               var params = {
                   type: 'POST',
                   url: '/admin/uploadImage',
                   data: formdata,
                   processData: false,
                   contentType: false,
                   success: function(result) {
                       var image_name = $.parseJSON(result).image_name;
                       var params1 = {
                           type: 'POST',
                           url: '/admin/updateMeta',
                           data: {
                               name: $(e).prop('name'),
                               value: image_name
                           },
                           success: function(result1) {}
                       }
                       $.ajax(params1);
                   }
               }
               $.ajax(params);
           }
       })
       if(_callback) _callback();
   }
</script>
@endsection
