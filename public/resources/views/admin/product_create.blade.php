@extends('admin.layout')
@section('header') Sản phẩm
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="box box-info">
                <div class="form-horizontal form-product">
                    <div class="box-title clearfix">
                        <div class="pull-left title-tab">Tạo sản phẩm
                            <div class="action_result">
                                <span style="color: red;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Tên sản phẩm<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12">
                                        <input name="name" placeholder="Tên sản phẩm"
                                               class="form-control title text-overflow-title">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Mô tả<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12">
                                        <textarea name="description" rows="8" cols="80" placeholder="Mô tả"
                                                  class="form-control title text-overflow-title"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Nội dung<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12">
                                        <textarea id="product_content" name="content" rows="8" cols="80"
                                                  placeholder="Mô tả"
                                                  class="tinymce form-control title text-overflow-title"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Giá bán<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12"><input type="text" min="0" name="product_price"
                                                                            placeholder="Giá bán"
                                                                            class="form-control title formatMoney"
                                                                            onkeypress="inputPositiveNumbers( event )")>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="tab-content">
                                <div class="form-group">
                                    <div class="col-lg-2 col-md-12 control-label">Giá so sánh<strong
                                                class="required">*</strong></div>
                                    <div class="col-lg-10 col-md-12"><input type="text" min="0"
                                                                            name="product_price_compare"
                                                                            placeholder="Giá so sánh"
                                                                            class="form-control title formatMoney"
                                                                            onkeypress="inputPositiveNumbers( event )")>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="box box-info">--}}
                {{--<div class="form-horizontal form-product">--}}
                    {{--<div class="box-title clearfix">--}}
                        {{--<div class="pull-left title-tab">Hình ảnh</div>--}}
                    {{--</div>--}}
                    {{--<div class="box-body">--}}
                        {{--<div class="tab-content">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-xs-12 product-images">--}}
                                    {{--<div class="col-xs-12 list-image choose-image-product">--}}
                                        {{--<div class="col-xs-3 col-sm-2 add-image"></div>--}}
                                    {{--</div>--}}
                                    {{--<input type="hidden" name="image" class="value featured-image"></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="pull-left title-tab">Quản lý tồn kho</div>
                    </div>
                    <div class="box-body">
                        <div class=""><input type="text" min="0" name="stock_manage"
                                             placeholder="Số lượng"
                                             class="form-control title">
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="pull-left title-tab">Nhóm sản phẩm</div>
                    </div>
                    <div class="box-body">
                        <select multiple="multiple" class="multiple-select settings" name="collection_ids">
                            @foreach (Helper::getAllCollection() as $collection)
                                <option value="{{$collection->id}}">{{$collection->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="pull-left title-tab">Hình đại diện</div>
                    </div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="form-group">
                                <div class="col-lg-4 col-md-12">
                                    <div class="image-preview" name="image">
                                        <label for="image-upload" class="image-label">Chọn hình</label>
                                        <input type="file" name="image" class="image-upload"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="pull-left title-tab">Trạng thái</div>
                    </div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="form-group">
                                <div class="col-lg-6 col-md-12">Trạng thái:<br>
                                    <input type="radio" name="status" value="active" checked="checked"><span
                                            class="label label-success">Sẵn
                                        sàng</span><br>
                                    <input type="radio" name="status" value="deleted"><span class="label label-danger">Bị
                                        xóa</span><br>
                                    <input type="radio" name="status" value="hiden"><span class="label label-warning">Bị
                                        ẩn</span><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="form-group">
                                <div class="col-lg-4 col-md-4 pull-right">
                                    <input type="button" id="product-create" value="Tạo"
                                           class="form-control title text-overflow-title btn btn-primary">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        tinymce.init({
            selector: 'textarea.tinymce',
            plugins: 'codesample'
        });
        $(function () {
            $('.multiple-select').multipleSelect({
                width: '100%'
            });
        });
        $(document).ready(function () {
            //init multile selects
            $.each($('select.multiple-select'), function (i, e) {
                var params = {
                    type: 'POST',
                    url: '/admin/getMeta',
                    data: {
                        name: $(e).attr('name')
                    },
                    success: function (result) {
                        if (result.code == 0) {
                            var ids = result.value.split(',');
                            $(e).multipleSelect('setSelects', ids);
                        }
                    }
                }
                $.ajax(params);
            })
        })
        //images product
        // $(document).on('click', '.product-images span.remove', function() {
        //     $(this).parent().remove();
        // });
        // data.images = '';
        // if (!data.images && $('.product-images .list-image .image').length) {
        //     data.images = $('.product-images .list-image .image').first().attr('data-name');
        // }
        // data.listImage = [];
        // $('.product-images').find('.list-image .image').each(function () {
        //     data.listImage.push($(this).attr('data-name'));
        // });

        $(document).ready(function () {
            $.uploadPreview({
                input_field: ".image-upload", // Default: .image-upload
                preview_box: ".image-preview", // Default: .image-preview
                label_field: ".image-label", // Default: .image-label
                label_default: "Chọn hình", // Default: Choose File
                label_selected: "", // Default: Change File
                no_label: true
            });
            $('#product-create').on('click', function () {
                try {
                    var name = $('input[name=name]').val();
                    var image = $('input[name=image]').prop('files')[0].name;
                    var description = $('textarea[name=description]').val();
                    var content = tinymce.get('product_content').getContent();
                    var status = $('input[name=status]:checked').val();
                    var price = $('input[name=product_price]').val();
                    var price_compare = $('input[name=product_price_compare]').val();
                    var stock_manage = $('input[name="stock_manage"]').val();
                    var collection_ids = $('select[name="collection_ids"]').val();
                } catch (e) {
                    $('.action_result span').text('Hãy nhập đầy đủ thông tin');
                    window.scrollTo(document.body.scrollWidth, 0);
                    return;
                }
                if (price == "" || price_compare == "") {
                    $('.action_result span').text('Hãy nhập giá của sản phẩm');
                    window.scrollTo(document.body.scrollWidth, 0);
                    return;
                }
                if (name == "" || description == "" || content == "" || status == "") {
                    $('.action_result span').text('Hãy nhập đầy đủ thông tin');
                    window.scrollTo(document.body.scrollWidth, 0);
                    return;
                }
                console.dir(collection_ids);
                var formdata = new FormData();
                var file = $('input[name=image]').prop('files')[0];
                formdata.append('image', file);
                var params1 = {
                    url: '/admin/uploadImage',
                    type: 'POST',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function (result1) {
                        var image = $.parseJSON(result1).image_name;
                        var params = {
                            url: '/admin/createProduct',
                            type: 'POST',
                            data: {
                                name: name,
                                image: image,
                                description: description,
                                content: content,
                                status: status,
                                price: price,
                                price_compare: price_compare,
                                stock_manage: stock_manage,
                                collection_ids: collection_ids
                            },
                            success: function (result) {
                                alert('Thêm thành công nhóm sản phẩm');
                                window.location.href = '/admin/product_all';
                            }
                        }
                        $.ajax(params);
                    }
                }
                $.ajax(params1);
            })
        })
    </script>
@endsection
