@extends('admin.layout')
@section('header') Mã giảm giá
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="box ">
                <div class="form-horizontal form-product">
                    <div class="box-title clearfix">
                        <div class="pull-left title-tab">Tạo mã giảm giá
                            <div class="action_result">
                                <span style="color: red;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="form-group">
                                <div class="col-lg-2 col-md-12 control-label">Tiêu đề<strong
                                            class="required">*</strong></div>
                                <div class="col-lg-10 col-md-12">
                                    <input name="title" placeholder="Tên mã giảm"
                                           class="form-control title text-overflow-title">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="form-group">
                                <div class="col-lg-2 col-md-12 control-label">Mô tả<strong
                                            class="required">*</strong></div>
                                <div class="col-lg-10 col-md-12">
                                        <textarea id="coupon_description" name="description" rows="8" cols="80"
                                                  placeholder="Mô tả"
                                                  class="tinymce form-control title text-overflow-title"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="col-xs-12 title">MÃ GIẢM GIÁ</div>
                    </div>
                    <div class="box-body">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <div class="text-title">Mã giảm giá<strong class="required">*</strong></div>
                                <input name="code" placeholder="Mã" class="form-control"></div>
                            <div class="form-group">
                                <div class="text-title">Số lượng<strong class="required">*</strong></div>
                                <input name="usage_left" placeholder="Số lượng" type="number" min="0" value="0"
                                       onkeypress="inputPositiveNumbers(event)" class="form-control"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="col-xs-12 title">THỂ LOẠI &amp; GIÁ TRỊ</div>
                    </div>
                    <div class="box-body">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <div class="text-title">Thể loại<strong class="required">*</strong></div>
                                <select name="type" class="form-control">
                                    <option value="value">Giảm theo đơn giá</option>
                                    <option value="percent">Giảm theo phần trăm</option>
                                </select></div>
                            <div class="form-group">
                                <div class="text-title">Giá trị<strong class="required">*</strong></div>
                                <input type="text" name="value" placeholder="Giá trị" min="0" value="0"
                                       onkeypress="inputPositiveNumbers(event)" class="form-control formatMoney"></div>
                            <div class="form-group max-value-percent hidden">
                                <div class="text-title">Giá trị tối đa<strong class="required">*</strong></div>
                                <input name="max_value_percent" placeholder="Giá trị tối đa"
                                       onkeypress="inputPositiveNumbers(event)" value="0"
                                       class="form-control formatMoney"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="form-horizontal">
                    <div class="box-title clearfix">
                        <div class="col-xs-12 title">TRẠNG THÁI</div>
                    </div>
                    <div class="box-body"><select name="status" class="form-control">
                            <option value="active">Hiển thị</option>
                            <option value="inactive">Ẩn</option>
                        </select></div>
                </div>
            </div>
        </div>
    </div>
@endsection