@extends('admin.layout')
@section('header')
<li class="hidden-xs"><a href="/admin/product_all">Sản phẩm</a></li>
<li class="title-ellipsis"><a>Danh sách</a></li>
@endsection
@section('content')
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="box box-info">
      <div class="form-horizontal form-product">
        <div class="box-header">
          <div class="box-title clearfix">
            <div class="pull-left title-tab">Danh sách sản phẩm
              @if  ($page_name == 'active')
              <span class="label label-success">Sẵn sàng</span>
              @elseif  ($page_name == 'deleted')
              <span class="label label-danger">Bị xóa</span>
              @elseif  ($page_name == 'hiden')
              <span class="label label-warning">Bị ẩn</span>
              @elseif  ($page_name == 'Tất cả')
              <span class="label label-info">Tất cả</span>
              @elseif  ((strpos($page_name, 'in_collection') !== false))
              <span class="label label-primary">Nhóm {{Helper::getCollection(explode('|', $page_name)[1])->title}}</span>
              @endif
              ({{$current_items}} trong tổng số {{$total_item}} sản phẩm)
            </div>
          </div>
        </div>

        <div class="box box-table">
          <div class="box-body">

            <div class="box-body">
              <div class="tab-content">
                <div class="form-group">
                  <div class="col-lg-2 col-md-6">
                    <strong>Chỉ hiện</strong>
                  </div>
                  <div class="col-lg-6 col-md-6">
                    <a href="/admin/product_all?sort=only-active"><span
                                                        class="label label-success">Sẵn
                                                    sàng</span> </a>|
                    <a href="/admin/product_all?sort=only-deleted"><span
                                                        class="label label-danger">Đã
                                                    xóa</span> </a>|
                    <a href="/admin/product_all?sort=only-hiden"><span
                                                        class="label label-warning">Đã
                                                    ẩn</span> </a>|
                    <a href="/admin/product_all"><span class="label label-info">Tất cả</span>
                                            </a>|
                    <strong>Nhóm</strong>
                    <select name="collection_id" class="form-control" style="display: inline-block; width: 100px;">
                                              <option value="all">Tất cả</option>
                                              @foreach (Helper::getAllCollection() as $collection)
                                                <option value="{{$collection->id}}"
                                                  @if (strpos($page_name, 'in_collection') !== false)
                                                    @if (explode('|', $page_name)[1] == $collection->id)
                                                      selected="selected"
                                                    @endif
                                                  @endif
                                                  >{{$collection->title}}</option>
                                              @endforeach
                                            </select>
                  </div>
                </div>


                <div class="form-group" id="actions" style="display: none">
                  <div class="col-lg-2 col-md-6">
                    <strong>Chuyển tất cả thành </strong>
                  </div>
                  <div class="col-lg-10 col-md-6">
                    <a href="#" class="action_name" data-value="active"><span
                                                        class="label label-success">Sẵn sàng</span> </a>|
                    <a href="#" class="action_name" data-value="deleted"><span
                                                        class="label label-danger">Đã xóa</span> </a>|
                    <a href="#" class="action_name" data-value="hiden"><span
                                                        class="label label-warning">Đã ẩn</span> </a>|
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-2 col-md-6">
                    <strong>Filter</strong>
                  </div>
                  <div class="col-lg-10 col-md-6">
                    <select class="form-control" name="filter" style="width: 225px;">
                      <option value="all">Tất cả</option>
                      <option value="price-asc">Giá tăng dần</option>
                      <option value="price-desc">Giá giảm dần</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                  <thead>
                    <th><input type="checkbox" id="checkall"/></th>
                    <th>#id</th>
                    <th>Tên sản phẩm</th>
                    <th>Hình ảnh</th>
                    <th>Giá</th>
                    <th>Ngày tạo</th>
                    <th>Trạng thái</th>
                    <th>Sửa</th>
                    <th>Xóa</th>
                  </thead>
                  <tbody>
                    @foreach  ($products as $product)
                    <tr class="item" id="{{$product->id}}">
                      <td><input type="checkbox" value="{{$product->id}}"
                                                           class="checkthis action"/></td>
                      <td>{{$product->id}}</td>
                      <td><a href="/admin/product?id={{$product->id}}">{{$product->title}}</a>
                      </td>
                      <td><img src="/uploads/{{$product->featured_image}}" alt=""
                                                         style="width: 50px; height: 50px;"></td>
                      <td>{{Helper::formatMoney($product->price)}} VNĐ</td>
                      <td>{{$product->created_at}}</td>
                      <td class="status">
                        @if  ($product->status == 'active')
                        <span class="label label-success">Sẵn sàng</span>
                        @elseif  ($product->status == 'deleted')
                        <span class="label label-danger">Bị xóa</span>
                        @else
                        <span class="label label-warning">Bị ẩn</span>
                        @endif
                      </td>
                      <td>
                        <p data-placement="top" data-toggle="tooltip" title="Edit">
                          <button class="btn btn-show-edit btn-primary btn-xs"
                                                                data-title="Edit" data-id="{{$product->id}}"><span
                                                                    class="glyphicon glyphicon-pencil"></span></button>
                        </p>
                      </td>
                      <td>
                        <p data-placement="top" data-toggle="tooltip" title="Delete">
                          <button class="btn btn-danger btn-xs btn-delete"
                                                                data-title="Delete"
                                                                data-id="{{$product->id}}" data-toggle="modal"
                                                                data-target="#delete"><span
                                                                    class="glyphicon glyphicon-trash"></span></button>
                        </p>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>

                <div class="col-lg-4 col-md-4 pull-left">
                  <button type="button" class="btn btn-success" name="button"
                          onclick="window.location.href='/admin/createProduct'">
                    Thêm sản phẩm
                  </button>
                </div>
                <div class="col-lg-8 col-md-8 pull-right">
                  <ul class="pagination pull-right">
                    <li class="disabled"><a href="#"><span
                                                            class="glyphicon glyphicon-chevron-left"></span></a></li>
                    @for  ($i=1; $i <= $total_page; $i++) <li class="
                  @if ($page == $i)
                                                        active
@endif
                                                        "><a
                                                            href="/admin/product_all?page={{$i}}&perpage={{$perpage}}">{{$i}}</a>
                      </li>
                      @endfor
                      <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
                      </li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                  <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                              </button>
                    <h4 class="modal-title custom_align" id="Heading">Sửa sơ bộ</h4>
                  </div>
                  <div class="modal-body">
                    <div class="form-group">
                      <div class="col-lg-4 col-md-12 control-label">Tên nhóm<strong
                                              class="required">*</strong></div>
                      <div class="col-lg-8 col-md-12"><input name="title"
                                                                         placeholder="Tên nhóm sản phẩm"
                                                                         class="settings form-control title text-overflow-title">
                                  </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-4 col-md-12 control-label">Hình đại diện<strong
                                              class="required">*</strong></div>
                        <div class="col-lg-8 col-md-12">
                          <div class="image-preview model-edit" data-type="collection" name="image">
                            <label for="image-upload" class="image-label">Chọn hình</label>
                            <input type="file" name="image" class="image-upload"/>
                                      </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-lg-4 col-md-12 control-label">Trạng thái<strong
                                              class="required">*</strong></div>
                          <div class="col-lg-8 col-md-12">
                            <select class="form-control" name="status">
                                          <option value="active">Sẵn sàng</option>
                                          <option value="deleted">Bị xóa</option>
                                          <option value="hiden">Bị ẩn</option>
                                      </select>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer ">
                        <button type="button" class="btn btn-primary btn-lg btn-quick-edit"
                                      style="width: 100%;"><span
                                          class="glyphicon glyphicon-ok-sign"></span> Update
                              </button>
                      </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      $(document).ready(function() {
        $.uploadPreview({
          input_field: ".image-upload", // Default: .image-upload
          preview_box: ".image-preview.model-edit", // Default: .image-preview
          label_field: ".image-label", // Default: .image-label
          label_default: "Chọn hình", // Default: Choose File
          label_selected: "", // Default: Change File
          no_label: true
        });
        $("#mytable #checkall").click(function() {
          if ($("#mytable #checkall").is(':checked')) {
            $("#mytable input[type=checkbox]").each(function() {
              $(this).prop("checked", true);
            });
          } else {
            $("#mytable input[type=checkbox]").each(function() {
              $(this).prop("checked", false);
            });
          }
        });
        $("[data-toggle=tooltip]").tooltip();
        $('.btn-delete').on('click', function(e) {
          e.preventDefault();
          var id = $(this).data('id');
          var params = {
            url: '/admin/deleteProduct',
            type: 'POST',
            data: {
              id: id
            },
            success: function(result) {
              if ($.parseJSON(result).code == 0) {
                $('tr#' + id + '').find('td.status').html('<span class="label label-danger">Bị xóa</span>');
                alert("Xóa thành công trang nội dung");
              }
            }
          }
          $.ajax(params);
        })
        $('.btn-show-edit').on('click', function(e) {
          var id = $(this).data('id');
          $.ajax({
            type: 'post',
            url: '/admin/getProductById',
            data: {
              id: id
            },
            success: function(result) {
              console.log(result);
              result = $.parseJSON(result);
              $('input[name=title]').val(result.data.title);
              $('select option[value=' + result.data.status + ']').attr('selected', 'selected');
              $('.image-preview.model-edit').css("background-image", "url(/uploads/" + result.data.featured_image + ")");
              $('.image-preview.model-edit').css("background-size", "cover");
              $('.image-preview.model-edit').css("background-position", "center center");
              $('#edit').attr('data-id', id);
              $('#edit').modal('show');
            }
          })
        })
        $('input.action, #checkall').on('click', function(e) {
          var action = false;
          $.each($('input.action'), function(i, e) {
            if ($(e).prop('checked')) {
              action = true;
              return;
            }
          })
          if (action) {
            $('#actions').css('display', 'block');
          } else {
            $('#actions').css('display', 'none');
          }
        })
        $('.action_name').on('click', function(e) {
          e.preventDefault();
          var status = $(this).data('value');
          var ids = $("input.action:checkbox:checked").map(function() {
            return $(this).val();
          }).get();
          $.ajax({
            url: '/admin/changeStatusProduct',
            type: "POST",
            data: {
              ids: ids,
              status: status
            },
            success: function(result) {
              if ($.parseJSON(result).code == 0) {
                console.dir($.parseJSON(result));
                window.location.reload(true);
              }
            }
          })
        })
      });
    </script>
    <script type="text/javascript">
      $("select[name=collection_id]").on('change', function(e) {
        window.location.href = "/admin/product_all?collection_id=" + $(this).val();
      })
      $("select[name=filter]").on('change', function(e) {
        var order_by = $(this).val().split("-")[0];
        var order_var = $(this).val().split("-")[1];
        if ($('select[name=collection_id]').val() != "all") {
          window.location.href = "/admin/product_all?collection_id=" + $('select[name=collection_id]').val() + "&order_by=" + order_by + "&order_var=" + order_var;
        } else {
          window.location.href = "/admin/product_all?sort=only-active&order_by=" + order_by + "&order_var=" + order_var;
        }
      })
    </script>
    @endsection
