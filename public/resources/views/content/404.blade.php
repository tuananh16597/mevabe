@extends('master')
@section('content')
    <div class="breadcrumb-wrapper">
        <div class="breadcrumb-title">
            <h1 class="page-title"><span>Trang bạn yêu cầu không tồn tại!</span></h1>
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="http://9736.chilishop.net/index.php?route=common/home">Trang chủ</a></li>
                    <li><a href="http://9736.chilishop.net/index.php?route=error/not_found">Trang bạn yêu cầu không tồn
                            tại!</a></li>
                </ul>
            </div>

        </div>
    </div>
    <div class="container">
        <section class="wraper_error">
            <div class="row"></div>
            <div class="position-display">
            </div>
        </section>
        <section class="wraper_error">

            <div class="row">
                <div class="col-sm-12">
                    <h1 class="error-page">404</h1>
                    <h2 class="error-page">Trang bạn yêu cầu không tồn tại!</h2>
                    <hr class="horizontal-break">
                    <a href="http://9736.chilishop.net/index.php?route=common/home">
                        <div class="btn btn-action">Tiếp tục</div>
                    </a>
                </div>
            </div>
        </section>
        <!-- end Illustration -->
        <!-- Button -->
    </div>
@endsection
