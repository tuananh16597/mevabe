@extends('master')
@section('content')
    <div class="breadcrumb-wrapper">
        <div class="breadcrumb-title">
            <h1 class="page-title"><span>Thông Tin Tài Khoản</span></h1>
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Trang chủ</a></li>
                    <li><a href="customer">Tài khoản</a></li>
                    <li class="active"><a href="#">Sửa thông
                            tin</a></li>
                </ul>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-12">
                <div class="position-display">
                </div>
                @if (!empty($action_result))
                <h3><strong style="color: red;">{{$action_result}}</strong></h3>
                @endif
                <form action="changeInfo" method="post"
                      enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <fieldset>
                        <legend>Thông tin cá nhân</legend>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-firstname">Tên: </label>
                            <div class="col-sm-10">
                                <input type="text" name="name" value="{{ $customer->name }}" placeholder="Tên:"
                                       id="input-firstname" class="form-control">
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-email">E-Mail:</label>
                            <div class="col-sm-10">
                                <input type="email" name="email" value="{{ $customer->email }}" placeholder="E-Mail:"
                                       id="input-email" class="form-control">
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-telephone">Điện thoại:</label>
                            <div class="col-sm-10">
                                <input type="tel" name="phone" value="{{ $customer->phone }}" placeholder="Điện thoại:"
                                       id="input-telephone" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <div class="buttons clearfix button-box">
                        <div class="pull-left"><a href="customer"
                                                  class="btn btn-default">Quay lại</a></div>
                        <div class="pull-right">
                            <input type="submit" value="Tiếp tục" class="btn btn-primary">
                        </div>
                    </div>
                </form>
                <div class="position-display">
                </div>
            </div>
        </div>
    </div>
@endsection