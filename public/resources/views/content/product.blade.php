@extends('master')
@section('content')
<div class="breadcrumb-wrapper">
  <div class="breadcrumb-title">
    <h1 class="page-title"><span>{{$product->title}}</span></h1>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="/">Trang chủ</a></li>
        @if (!empty($product->collection))
        <li><a href="collection?id={{$product->collection->id}}">{{$product->collection->id}}</a></li>
        @endif
        <li class="active"><a href="product?id={{$product->id}}">{{$product->title}}</a></li>
      </ul>
    </div>

  </div>
</div>
<div class="container product-ms">
  <div class="row item_success">
    <!--dont delete -> this is alert success ad product cart -->
  </div>
  <div class="row">
    <div id="content" class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
      <div class="position-display">
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="single-product-image">
            <div class="single-pro-main-image">
              <a href="product?id={{$product->id}}" title="Thỏ bông thân thiện"><img id="optima_zoom" src="uploads/{{$product->featured_image}}" data-zoom-image="uploads/{{$product->featured_image}}" title="{{$product->title}}" alt="{{$product->title}}" class="img-responsive" /></a>
            </div>
            <div class="single-pro-thumb">
              <ul class="thubm-caro" id="optima_gallery">
                <li>
                  <a href="product?id={{$product->id}}" title="{{$product->title}}" data-image="uploads/{{$product->featured_image}}" data-zoom-image="uploads/{{$product->featured_image}}"> <img class="img-responsive" src="uploads/{{$product->featured_image}}" title="{{$product->title}}" alt="{{$product->title}}" /> </a>
                </li>
                @if (!empty($product->images))
                  @foreach ($product->images as $image)
                    <li>
                      <a href="product?id={{$product->id}}" title="{{$product->title}}" data-image="uploads/{{$image->name}}" data-zoom-image="uploads/{{$image->name}}"> <img class="img-responsive" src="uploads/{{$image->name}}" title="{{$product->title}}" alt="{{$product->title}}" /> </a>
                    </li>
                  @endforeach
                @endif
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="single-product-description">
            <div class="pro-desc">
              <h2><strong>{{$product->title}}</strong></h2>
              <ul class="list-unstyled">
                <li>Mã sản phẩm: <span class="span">{{$product->id}}</span></li>
                {{--<li>Số lượng sản phẩm trong kho: <span class="span">{{$product->stock_quant}}</span>--}}
                </li>
              </ul>
              <ul class="list-unstyled des2">
                <li class="span">Mô tả</li>
                <li>{!!$product->description!!}</li>
              </ul>
            </div>
            <!--  <div class="pro-desc">-->
            <ul class="list-unstyled">

              <li><span style="text-decoration: line-through;" class="old-price">{{Helper::formatMoney($product->price_compare)}} VNĐ</span></li>
              <li>
                <span class="regular-price span_red">{{Helper::formatMoney($product->price)}} VNĐ</span>
              </li>

            </ul>
          </div>
          <div id="product">
            <form action="addToCart" method="post">

              @csrf
              <h3>Số lượng</h3> {{--
              <div class="form-group required">
                <label class="control-label">Kích thước</label>
                <div id="input-option26">
                  <div class="radio">
                    <label class="fl_left">
                                                  <input type="radio" name="option[26]" value="114">
                                                  Nhỏ                                                                                                            </label>
                  </div>
                  <div class="radio">
                    <label class="fl_left">
                                                  <input type="radio" name="option[26]" value="117">
                                                  Trung bình                                                                                                            </label>
                  </div>
                  <div class="radio">
                    <label class="fl_left">
                                                  <input type="radio" name="option[26]" value="116">
                                                  Lớn                                                                                                            </label>
                  </div>
                </div>
              </div> --}}
              <div class="product_details_cart">
                <div class="product-quantity">
                  <div class="numbers-row">
                    <input type="text" name="quantity" value="1" id="product_quantity">
                    <input type="hidden" name="product_id" value="56">
                    <div class="dec qtybutton btn-minus">-</div>
                    <div class="inc qtybutton btn-plus">+</div>
                  </div>
                  <script type="text/javascript">
                  $('.btn-minus').on('click', function(e) {
                    e.preventDefault();
                    if ($(this).parent().find('input[name=quantity]').val() > 1)
                    $(this).parent().find('input[name=quantity]').val($(this).parent().find('input[name=quantity]').val() - 1);
                    $(this).parent().find('input[name=quantity]').trigger('change');
                  });
                  $('.btn-plus').on('click', function(e) {
                    e.preventDefault();
                    $(this).parent().find('input[name=quantity]').val(parseInt($(this).parent().find('input[name=quantity]').val()) + 1);
                    $(this).parent().find('input[name=quantity]').trigger('change');
                  });
                  </script>

                  {{--
                  <div class="fv-comp-button">
                    <ul class="add-to-links">
                      <li><button type="button" data-toggle="tooltip" class="link-wishlist" title="" onclick="compare.add('56');" data-original-title="So sánh"><i class="fa fa-retweet"></i></button></li>
                      <li><button type="button" data-toggle="tooltip" class="link-wishlist" title="" onclick="wishlist.add('56');" data-original-title="Yêu thích"><i class="fa fa-heart-o"></i></button></li>
                    </ul>
                  </div> --}}
                </div>
                <div class="single-poraduct-botton">
                  <button type="submit" data-loading-text="Đang Xử lý..." data-id="{{$product->id}}" class="add-btn has-quantity push_button button shopng-btn">Thêm vào giỏ</button>
                </div>
              </div>
            </form>
            <div class="product-quantity">
              <!-- AddThis Button BEGIN -->
              <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
              <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
              <!-- AddThis Button END -->
            </div>

            {{-- <p class="tags-ms">
              <label>Xu hướng tìm kiếm:</label>
              <a href="http://9736.chilishop.net/index.php?route=product/search&amp;tag=Quà tặng nhồi bông">Quà tặng nhồi bông</a>
            </p> --}}
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="bg-ms-product">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab-description" data-toggle="tab">Mô tả</a></li>
              {{--
              <li><a href="#tab-review" data-toggle="tab">Đánh giá (0)</a></li> --}}
            </ul>
            <div class="tab-content">
              <div class="tab-pane active bottom20" id="tab-description">
               {!!$product->content!!}
              </div>

            </div>
          </div>
        </div>
      </div>

    </div>
    <div id="column-right" class="col-sm-12 col-md-3 col-lg-3 col-xs-12">
      <div class="dv-builder-full">
        <div class="dv-builder list_product">
          <div class="dv-module-content">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                <div class="dv-item-module ">
                  <div class="product_module special_product">
                    <h3 class="title title_special">Khuyến mãi</h3>
                    <div class="product-layout-custom">
                      @php $sale_products = Helper::getProducts(explode(",", Helper::getMeta('sale_products')));
                      @endphp
                      @if (!empty($sale_products))
                      @foreach ($sale_products as $sale_product)
                      <div class="medium">
                        <div class="product-thumb transition">
                          <div class="image"><a href="product?id={{$sale_product->id}}"><img
                                                                            style="width: 60px; height: 75px;"
                                                                            src="uploads/{{$sale_product->featured_image}}"
                                                                            alt="{{$sale_product->featured_image}}"
                                                                            title="{{$sale_product->featured_image}}"
                                                                            class="img-responsive"></a> {{--
                            <div class="button-group">
                              <button type="button" data-toggle="tooltip" title="" onclick="wishlist.add('55');" data-original-title="Yêu thích"><i class="fa fa-heart"></i>
                                                                    <span>Yêu thích</span>
                                                                  </button>
                              <button type="button" data-toggle="tooltip" title="" onclick="compare.add('55');" data-original-title="So sánh"><i class="fa fa-exchange"></i>
                                                                    <span>So sánh</span>
                                                                  </button>
                            </div> --}}
                          </div>
                          <div class="caption">
                            <h4>
                                                                    <a href="product?id={{$sale_product->id}}">{{$sale_product->title}}</a>
                                                                </h4>
                            <p class="description">{{$sale_product->description}}</p>
                            <div class="rating">
                              <span class="fa fa-stack"><i
                                                                                class="fa fa-star-o"></i></span>
                              <span class="fa fa-stack"><i
                                                                                class="fa fa-star-o"></i></span>
                              <span class="fa fa-stack"><i
                                                                                class="fa fa-star-o"></i></span>
                              <span class="fa fa-stack"><i
                                                                                class="fa fa-star-o"></i></span>
                              <span class="fa fa-stack"><i
                                                                                class="fa fa-star-o"></i></span>
                            </div>
                            <p class="price">
                              <span class="price-old">{{Helper::formatMoney($sale_product->price_compare)}}
                                                                        VNĐ</span>
                              <br>
                              <span class="price-new">{{Helper::formatMoney($sale_product->price)}}
                                                                        VNĐ</span>
                            </p>
                          </div>
                        </div>
                      </div>
                      @endforeach
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <style>
  </style>
  <div class="product-late-ms product_module">
    <div class="row">
      <div class="col-sm-12">
        <div class="area-title">
          <h3 class="text_related title"><span>Sản phẩm liên quan</span></h3>
          <div class="titleborderOut">
            <div class="titleborder"></div>
          </div>
        </div>
      </div>
      <div class="featured-item">
        @if (!empty($product->related_products))
        @foreach ($product->related_products as $related_product)
        <div class="medium">
          <div class="product-thumb transition">
            <div class="image"><a href="product?id={{$related_product->id}}"><img
                                                src="uploads/{{$related_product->featured_image}}"
                                                alt="{{$related_product->title}}" title="{{$related_product->title}}"
                                                class="img-responsive"/></a> {{--
              <div class="button-group">
                <button type="button" data-toggle="tooltip" title="Yêu thích" onclick="wishlist.add('3');">
                                    <span>Yêu thích</span>
                                  </button>
                <button type="button" data-toggle="tooltip" title="So sánh" onclick="compare.add('3');">
                                  <span>So sánh</span>
                                </button>
              </div> --}}
            </div>

            <div class="caption">
              <h4><a href="product?id={{$related_product->id}}">{{$related_product->title}}</a>
                                    </h4>
              <p class="description">{{$related_product->description}}</p>
              {{--
              <div class="rating">
                <span class="fa fa-stack"><i class="fa fa-star-o"></i></span>
                <span class="fa fa-stack"><i class="fa fa-star-o"></i></span>
                <span class="fa fa-stack"><i class="fa fa-star-o"></i></span>
                <span class="fa fa-stack"><i class="fa fa-star-o"></i></span>
                <span class="fa fa-stack"><i class="fa fa-star-o"></i></span>
              </div> --}}
              <p class="price">
                <span class="price-old">{{Helper::formatMoney($related_product->price_compare)}} VNĐ</span>
                </br>
                <span class="price-new">{{Helper::formatMoney($related_product->price)}} VNĐ</span>

              </p>
            </div>
            <div class="button-group-cart">
              <button type="button" data-id="{{$related_product->id}}" data-toggle="tooltip" title="Thêm vào giỏ"><span>Thêm vào giỏ</span></button>
            </div>
          </div>
        </div>
        @endforeach
        @endif
      </div>
    </div>
  </div>
</div>
<script>
  $('.add-btn').on('click', function(e) {
    e.preventDefault();
    var quantity = 1;
    var id = $(this).data('id');
    if($(this).hasClass("has-quantity")) {
      quantity = $('[name=quantity]').val();
    }
    StoreAPI.addItem(id, quantity, function(result) {
        // console.dir(result);
        alert("Thêm thành công " + quantity + " sản phẩm vào giỏ hàng!");
      setTimeout(function () {
          StoreAPI.getCart(function (result) {
              var items = result.data.items;
              var count = 0;
              $.each(items, function (i, e) {
                  count += e.quantity;
              })
              $('.num_product').html(parseInt(count));
          })
      }, 1000);
        location.reload();
    });
  })
  $(document).ready(function() {
      $('.button-group-cart button').on('click', function () {
          id = $(this).attr("data-id");
          window.location.href = "/product?id=" + id;
      })
    /*---- -----*/
    $('.thubm-caro').owlCarousel({
      items: 4,
      pagination: false,
      navigation: true,
      autoPlay: false,
      itemsDesktop: [1199, 3],
      itemsDesktopSmall: [991, 3],
      itemsTablet: [767, 3],
      itemsMobile: [480, 2],
      navigationText: ['<i class="fa fa-angle-left owl-prev-icon"></i>', '<i class="fa fa-angle-right owl-next-icon"></i>']
    });
    $('.product-late-ms .featured-item').owlCarousel({
      navigation: true,
      pagination: false,
      items: 4,
      itemsDesktop: [1199, 3],
      itemsDesktopSmall: [979, 3],
      itemsTablet: [767, 2],
      itemsMobile: [480, 1],
      navigationText: ['<div class="slide_arrow_prev"><i class="fa fa-angle-left"></i></div>', '<div class="slide_arrow_next"><i class="fa fa-angle-right"></i></div>']
    });
  })
</script>
@endsection
