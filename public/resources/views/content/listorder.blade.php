@extends('master')
@section('content')
    <div class="breadcrumb-wrapper">
        <div class="breadcrumb-title">
            <h1 class="page-title"><span>Lịch Sử Đặt Hàng</span></h1>
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Trang chủ</a></li>
                    <li><a href="/customer">Tài khoản</a></li>
                    <li class="active"><a href="">Lịch Sử Đặt
                            Hàng</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @php
                $orders = Helper::getOrderByCustomerID($customer->id)
            @endphp
            @if(count($orders) == 0)
                <div id="content" class="col-sm-12">
                    <div class="position-display">
                    </div>
                    <p>Bạn chưa có đơn hàng nào!</p>
                    <div class="buttons clearfix button-box">
                        <div class="pull-right">
                            <a href="/" class="btn btn-primary">Tiếp tục</a>
                        </div>
                    </div>
                    <div class="position-display">
                    </div>
                </div>
            @else
                <div id="content" class="col-sm-12">
                    <div class="position-display">
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <td class="text-right">Mã đơn hàng</td>
                                <td class="text-left">Tình trạng</td>
                                <td class="text-left">Ngày tạo</td>
                                <td class="text-right">Số lượng Đặt mua</td>
                                <td class="text-left">Khách Hàng</td>
                                <td class="text-right">Tổng Cộng</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td class="text-right">#{{ $order->id }}</td>
                                    <td class="text-left">{{ $order->status }}</td>
                                    <td class="text-left">{{ $order->created_at }}</td>
                                    <td class="text-right"></td>
                                    <td class="text-left">nguyen minh thiuest</td>
                                    <td class="text-right">340.000 VNĐ</td>
                                    <td class="text-right"><a
                                                href="http://9736.chilishop.net/index.php?route=account/order/info&amp;order_id=27"
                                                data-toggle="tooltip" title="" class="btn btn-info"
                                                data-original-title="Xem"><i
                                                    class="fa fa-eye"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="text-right"></div>
                    <div class="buttons clearfix button-box">
                        <div class="pull-right"><a href="/"
                                                   class="btn btn-primary">Tiếp tục</a></div>
                    </div>
                    <div class="position-display">
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection