@extends('master')
@section('content')
    <div class="breadcrumb-wrapper">
        <div class="breadcrumb-title">
            <h1 class="page-title"><span>Tài khoản của bạn đã được tạo!</span></h1>
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Trang chủ</a></li>
                    <li><a href="/customer">Tài khoản</a></li>
                    <li class="active"><a href="#">Thành công</a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-12">
                <div class="position-display">
                </div>
                <h1>Tài khoản của bạn đã được tạo!</h1>
                <h3>Xin chúc mừng! Tài khoản Mua hàng của bạn đã tạo thành công!</h3>
                <h3>
                    <div class="buttons">
                        <div class="pull-right"><a href="/"
                                                   class="btn btn-primary">Tiếp tục</a></div>
                    </div>
                    <div class="position-display">
                    </div>
                </h3>
            </div>
        </div>
    </div>
@endsection