-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th9 25, 2018 lúc 09:17 AM
-- Phiên bản máy phục vụ: 10.1.32-MariaDB
-- Phiên bản PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `db_mevabe`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `collection`
--

CREATE TABLE `collection` (
  `id` int(10) NOT NULL,
  `title` varchar(225) CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(255) NOT NULL,
  `view` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin7;

--
-- Đang đổ dữ liệu cho bảng `collection`
--

INSERT INTO `collection` (`id`, `title`, `image`, `description`, `content`, `created_at`, `updated_at`, `status`, `view`) VALUES
(12, 'cms', '1537024716Screenshot from 2018-09-10 21-28-27.png', '123', '<p>123</p>', '2018-09-25 03:06:41', '2018-09-25 03:06:41', 'active', 6),
(13, 'cms1', '1537024732Screenshot from 2018-09-10 21-28-27.png', '123', '<p>123</p>', '2018-09-25 00:33:04', '2018-09-25 00:33:04', 'active', 6),
(14, 'cms2', '1537024764Screenshot from 2018-09-10 21-28-27.png', '123', '<p>123</p>', '2018-09-25 00:33:01', '2018-09-25 00:33:01', 'active', 6),
(15, '123', '1537153418Screenshot%20from%202018-09-01%2011-19-00.png', '123', '<p>123</p>', '2018-09-25 00:33:09', '2018-09-25 00:33:09', 'active', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `collection_product`
--

CREATE TABLE `collection_product` (
  `id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `collection_product`
--

INSERT INTO `collection_product` (`id`, `collection_id`, `product_id`, `created_at`, `updated_at`, `type`) VALUES
(10, 14, 30, '2018-09-17 02:58:01', '2018-09-17 02:51:59', 'product'),
(11, 13, 30, '2018-09-17 02:58:04', '2018-09-17 02:51:59', 'product'),
(14, 14, 32, '2018-09-17 03:00:14', '2018-09-17 03:00:14', 'product'),
(15, 13, 32, '2018-09-17 03:00:14', '2018-09-17 03:00:14', 'product'),
(16, 12, 32, '2018-09-17 03:00:14', '2018-09-17 03:00:14', 'product'),
(21, 15, 32, '2018-09-17 03:14:24', '2018-09-17 03:14:24', 'collection'),
(22, 15, 33, '2018-09-18 04:02:48', '2018-09-18 04:02:48', 'product'),
(23, 14, 34, '2018-09-23 02:11:15', '2018-09-23 02:11:15', 'product'),
(24, 13, 34, '2018-09-23 02:11:15', '2018-09-23 02:11:15', 'product'),
(25, 12, 34, '2018-09-23 02:11:15', '2018-09-23 02:11:15', 'product');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `phone` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `content`, `phone`, `status`, `created_at`, `updated_at`) VALUES
(2, 'thanh minh nguyen', 'thanhminh9p@gmail.com', 'website rat tuyet cac ban a', '', 'Read', '2018-09-24 04:43:57', '2018-09-24 04:43:57'),
(3, 'nguyen thanh min', 'thanhminh9p@gmail.com', 'thanh minh muon contact', '0935829538', 'Unread', '2018-09-25 03:28:22', '2018-09-25 03:28:22'),
(4, 'minhnuyen', 'thanhminh9p@gmail.com', 'muon contaact', '0935829538', 'Read', '2018-09-25 03:27:57', '2018-09-25 03:27:57'),
(5, 'nguyen thanh minh', 'thanhminh9p@gmail.com', 'minh muon mua hang', '0935829588', 'Unread', '2018-09-25 03:27:10', '2018-09-25 03:27:10');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `coupon`
--

CREATE TABLE `coupon` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `minimum` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `usage_left` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `coupon`
--

INSERT INTO `coupon` (`id`, `title`, `description`, `code`, `status`, `start_date`, `end_date`, `created_at`, `updated_at`, `minimum`, `discount`, `usage_left`) VALUES
(1, 'ILOVEM&BSHOP', 'Mã giảm khuyến mãi khai trương shop', '123123', 'using', '2018-09-26', '2018-09-29', '2018-09-25', '2018-09-25', 100000, 50000, 10);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8 NOT NULL,
  `gender` varchar(255) CHARACTER SET utf8 NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin7;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`id`, `name`, `phone`, `gender`, `address`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Thanh minh 1', 'Phone', 'nam', 'Address', 'test', 'test', '2018-09-21 03:31:00', '2018-09-21 03:31:00'),
(2, 'nguyen minh', '0935829538', '', '12 dong bac', 'thanhminh9p@gmail.com', '123456', '2018-09-25 03:08:11', '2018-09-25 03:08:11');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `image`
--

INSERT INTO `image` (`id`, `name`, `type_id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'test.jpg', 1, 'product', '2018-08-21 09:35:18', '0000-00-00 00:00:00'),
(2, 'test.jpg', 1, 'product', '2018-08-21 09:35:30', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `meta`
--

CREATE TABLE `meta` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value` text CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin7;

--
-- Đang đổ dữ liệu cho bảng `meta`
--

INSERT INTO `meta` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'sale_products', '1,2,3,4', '2018-08-17 14:37:29', '0000-00-00 00:00:00'),
(2, 'sale_collection', '1,2,3', '2018-08-17 14:48:04', '0000-00-00 00:00:00'),
(3, 'san-pham-noi-bat', '3,2,1', '2018-09-02 09:05:38', '2018-09-02 09:05:38'),
(4, 'best-sale', '3,2', '2018-09-02 09:05:38', '2018-09-02 09:05:38'),
(17, 'shop-name', 'Shop đủ má đỉnh', '2018-09-23 02:29:01', '2018-09-23 02:29:01'),
(18, 'phone', '01294 463 179', '2018-09-23 02:29:01', '2018-09-23 02:29:01'),
(19, 'image', '153521326425299276_1507771559342083_4719443541609053360_n.jpg', '2018-08-25 16:07:45', '2018-08-25 16:07:45'),
(20, 'favicon', '1537665275favicon.png', '2018-09-23 02:29:00', '2018-09-23 02:29:00'),
(21, 'logo', '1537665275logo.png', '2018-09-23 02:29:00', '2018-09-23 02:29:00'),
(22, 'address', 'Số 123, Đường ABC, Quận ABC, Thành Phố Hồ Chí Minh', '2018-09-23 02:29:02', '2018-09-23 02:29:02'),
(23, 'slogan', 'Đồ chơi trẻ em giúp cho các bé vừa được vui chơi, vừa được phát triển trí tưởng tượng, tư duy logic và rèn luyện sự khéo léo cho đôi tay khi xếp những mảnh ghép.', '2018-09-23 02:29:02', '2018-09-23 02:29:02'),
(24, 'youtube', 'https://www.youtube.com/watch?v=y7wlf3W9cy4', '2018-09-23 02:29:01', '2018-09-23 02:29:01'),
(25, 'twitter', 'https://www.youtube.com/watch?v=y7wlf3W9cy4', '2018-09-23 02:29:02', '2018-09-23 02:29:02'),
(26, 'facebook', 'https://www.youtube.com/watch?v=7pJLVPpdjlM', '2018-09-23 02:29:02', '2018-09-23 02:29:02'),
(27, 'rss', 'https://www.youtube.com/watch?v=7pJLVPpdjlM', '2018-09-23 02:29:02', '2018-09-23 02:29:02'),
(28, 'working_time', 'Thứ 2 - 6: 7h - 11h', '2018-09-23 02:29:02', '2018-09-23 02:29:02'),
(29, 'powerby', 'nguyen nguyen', '2018-09-23 02:29:03', '2018-09-23 02:29:03'),
(30, 'nhom-noi-bat', '3,2,1', '2018-09-02 09:05:39', '2018-09-02 09:05:39'),
(31, 'san-pham-moi-nhat', '5,1,2,4,3', '2018-09-02 09:05:39', '2018-09-02 09:05:39'),
(32, 'san-pham-ban-chay', '5,1,2,4,3', '2018-09-02 09:05:39', '2018-09-02 09:05:39'),
(33, 'san-pham-khuyen-mai', '5,1,2,4,3', '2018-09-02 09:05:39', '2018-09-02 09:05:39'),
(34, 'copyright', 'nguyen minh', '2018-09-23 02:29:03', '2018-09-23 02:29:03'),
(35, 'service_1', 'MIỄN PHÍ VẬN CHUYỂN ĐƠN HÀNG', '2018-09-23 02:29:03', '2018-09-23 02:29:03'),
(36, 'service_2', 'HỖ TRỢ KHÁCH HÀNG 24/7', '2018-09-23 02:29:03', '2018-09-23 02:29:03'),
(37, 'showcase', '1', '2018-08-28 11:00:32', '2018-08-28 11:00:32'),
(38, 'service_3', 'TƯ VẤN TRỰC TUYẾN', '2018-09-23 02:29:03', '2018-09-23 02:29:03'),
(39, 'slide_1', '1537665208Slider1.jpg', '2018-09-23 02:29:00', '2018-09-23 02:29:00'),
(40, 'slide_2', '1537665209Slider2.jpg', '2018-09-23 02:29:00', '2018-09-23 02:29:00'),
(41, 'slide_4', '1537665210Slider4.jpg', '2018-09-23 02:29:00', '2018-09-23 02:29:00'),
(42, 'slide_3', '1537665209Slider3.jpg', '2018-09-23 02:29:00', '2018-09-23 02:29:00'),
(43, 'brand_1', '1535505610startropics_by_doctor_g-db18fkb.png', '2018-09-23 02:29:01', '2018-09-23 02:29:01'),
(44, 'brand_2', '1535508317no_man_s_sky_2018_video_game-wallpaper-1920x1080.jpg', '2018-09-23 02:29:01', '2018-09-23 02:29:01'),
(45, 'brand_3', '1535508472noire_girl-wallpaper-1920x1080.jpg', '2018-09-23 02:29:01', '2018-09-23 02:29:01'),
(46, 'brand_4', '153562082936436108_136555870564145_1113930112161546240_n.jpg', '2018-09-23 02:29:01', '2018-09-23 02:29:01'),
(47, 'desktop-colllections', '14,13,12', '2018-09-23 02:29:03', '2018-09-23 02:29:03'),
(48, 'hot-products', '34', '2018-09-23 02:29:04', '2018-09-23 02:29:04'),
(49, 'special-collection', '14,13,15', '2018-09-23 02:29:04', '2018-09-23 02:29:04'),
(50, 'new-products', '34', '2018-09-23 02:29:04', '2018-09-23 02:29:04'),
(51, 'sale-products', '34', '2018-09-23 02:29:04', '2018-09-23 02:29:04'),
(52, 'promotion-products', '34', '2018-09-23 02:29:04', '2018-09-23 02:29:04');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `payment_method` varchar(255) CHARACTER SET utf8 NOT NULL,
  `order_status` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `total` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `receiver_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `receiver_phone` varchar(255) CHARACTER SET utf8 NOT NULL,
  `receiver_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin7;

--
-- Đang đổ dữ liệu cho bảng `order`
--

INSERT INTO `order` (`id`, `customer_id`, `payment_method`, `order_status`, `total`, `discount`, `receiver_name`, `receiver_phone`, `receiver_address`, `created_at`, `updated_at`) VALUES
(27, 1, 'cod', 'confirmed', 4700000, 0, 'Nguoi nhan', '0123123', 'Dia chi nguoi nhan', '2018-09-01 08:50:39', '2018-09-01 08:50:39'),
(28, 1, 'cod', 'new', 20002, 0, 'Thanh minh 1', 'Phone', 'Address', '2018-09-04 10:08:26', '2018-09-04 10:08:26'),
(29, 1, 'cod', 'new', 12058530, 20000, 'nguyen tuan tu', '091247532', '12 dong bac tan chanh hiep', '2018-09-18 04:23:51', '2018-09-18 04:23:51'),
(30, 2, 'bank_transfer', 'new', 60212650, 20000, 'nguyen tuan tu', '091258731', '123 CMT8', '2018-09-19 00:48:49', '2018-09-19 00:48:49'),
(31, 1, 'cod', 'new', 12058530, 20000, 'Thanh minh 1', 'Phone', 'Address', '2018-09-21 02:45:59', '2018-09-21 02:45:59'),
(32, 1, 'cod', 'new', 12058530, 20000, 'Thanh minh 1', 'Phone', 'Address', '2018-09-21 02:46:24', '2018-09-21 02:46:24'),
(33, 1, 'cod', 'cancel', 12058530, 20000, 'Thanh minh 1', 'Phone', 'Address', '2018-09-23 03:05:50', '2018-09-23 03:05:50'),
(34, 1, 'cod', 'cancel', 20123, 20000, 'Thanh minh 1', 'Phone', 'Address', '2018-09-23 03:05:21', '2018-09-23 03:05:21'),
(35, 1, 'cod', 'paid', 20123, 1000, 'Thanh minh 1', 'Phone', 'Address', '2018-09-23 03:04:28', '2018-09-23 03:04:28'),
(36, 2, 'cod', 'new', 260000, 1000, 'nguyen tuan tu', '09125852', '123 CMT8 quan 1', '2018-09-24 02:39:31', '2018-09-24 02:39:31'),
(37, 2, 'cod', 'new', 260000, 1000, 'nguyen tuan tu', '09125852', '123 CMT8 quan 1', '2018-09-24 02:39:38', '2018-09-24 02:39:38'),
(38, 2, 'cod', 'new', 260000, 1000, 'nguyen minh', '0283569236', '12 dong bac', '2018-09-24 02:48:43', '2018-09-24 02:48:43'),
(39, 2, 'cod', 'new', 340000, 0, 'nguyen minh', '0283569236', '12 dong bac', '2018-09-24 04:12:40', '2018-09-24 04:12:40'),
(40, 2, 'cod', 'new', 100000, 0, 'nguyen minh', '0283569236', '12 dong bac', '2018-09-24 09:10:39', '2018-09-24 09:10:39'),
(41, 2, 'bank_transfer', 'new', 420000, 0, 'Nguyen tuan', '039404067', 'CMT8 Q1 Tp Ho Chi Minh', '2018-09-25 03:00:04', '2018-09-25 03:00:04'),
(42, 2, 'bank_transfer', 'confirmed', 420000, 0, 'Nguyen tuan', '039404067', 'CMT8 Q1 Tp Ho Chi Minh', '2018-09-25 00:47:19', '2018-09-25 00:47:19'),
(43, 2, 'bank_transfer', 'confirmed', 420000, 0, 'Nguyen tuan', '039404067', 'CMT8 Q1 Tp Ho Chi Minh', '2018-09-25 00:47:00', '2018-09-25 00:47:00'),
(44, 2, 'cod', 'confirmed', 420000, 0, 'nguyen tuan', '039404067', 'CMT8 Q1 tp hcm', '2018-09-25 00:44:40', '2018-09-25 00:44:40'),
(45, 2, 'cod', 'new', 100000, 0, 'nguyen minh', '0935829538', '12 dong bac', '2018-09-25 03:00:10', '2018-09-25 03:00:10'),
(46, 2, 'cod', 'confirmed', 100000, 0, 'nguyen minh', '0935829538', '12 dong bac', '2018-09-25 02:33:52', '2018-09-25 02:33:52'),
(47, 2, 'cod', 'new', 100000, 0, 'nguyen minh', '0935829538', '12 dong bac', '2018-09-25 03:06:16', '2018-09-25 03:06:16'),
(48, 2, 'cod', 'new', 100000, 0, 'nguyen minh', '0935829538', '12 dong bac', '2018-09-25 03:07:30', '2018-09-25 03:07:30'),
(49, 2, 'cod', 'new', 100000, 0, 'nguyen minh', '0935829538', '12 dong bac', '2018-09-25 03:07:49', '2018-09-25 03:07:49'),
(50, 2, 'cod', 'new', 100000, 0, 'nguyen minh', '0935829538', '12 dong bac', '2018-09-25 03:08:11', '2018-09-25 03:08:11');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unit_price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin7;

--
-- Đang đổ dữ liệu cho bảng `order_detail`
--

INSERT INTO `order_detail` (`id`, `order_id`, `product_id`, `quantity`, `created_at`, `updated_at`, `unit_price`) VALUES
(33, 27, 5, 2, '2018-09-01 07:59:21', '2018-09-01 07:59:21', 2350000),
(34, 28, 3, 2, '2018-09-04 10:08:26', '2018-09-04 10:08:26', 10001),
(35, 29, 33, 1, '2018-09-18 04:23:51', '2018-09-18 04:23:51', 12038530),
(36, 30, 33, 5, '2018-09-19 00:48:49', '2018-09-19 00:48:49', 12038530),
(37, 33, 33, 1, '2018-09-21 02:49:47', '2018-09-21 02:49:47', 12038530),
(38, 34, 30, 1, '2018-09-21 02:50:49', '2018-09-21 02:50:49', 123),
(39, 35, 30, 1, '2018-09-21 03:31:00', '2018-09-21 03:31:00', 123),
(40, 39, 34, 4, '2018-09-24 04:12:40', '2018-09-24 04:12:40', 80000),
(41, 40, 34, 1, '2018-09-24 09:10:39', '2018-09-24 09:10:39', 80000),
(42, 44, 34, 5, '2018-09-25 00:41:42', '2018-09-25 00:41:42', 80000),
(43, 45, 34, 1, '2018-09-25 02:29:25', '2018-09-25 02:29:25', 80000),
(44, 47, 34, 1, '2018-09-25 03:06:16', '2018-09-25 03:06:16', 80000),
(45, 50, 34, 1, '2018-09-25 03:08:11', '2018-09-25 03:08:11', 80000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL,
  `status` varchar(255) CHARACTER SET utf32 NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `page`
--

INSERT INTO `page` (`id`, `title`, `description`, `content`, `status`, `created_at`, `updated_at`) VALUES
(1, 'giới thiệu edit', 'đây là giới thiệu', '<p>giới thiệu n&egrave; man</p>', 'active', '2018-09-01 08:05:56', '2018-09-01 08:05:56'),
(2, 'huong dan thanh toan', 'thanh toan tao bay cho nee', '<p>day laf thanh toan</p>', 'active', '2018-08-30 00:51:29', '2018-08-30 00:51:29');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `stock_quant` int(11) NOT NULL,
  `price` bigint(20) NOT NULL,
  `price_compare` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `featured_image` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `view` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin7;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `content`, `stock_quant`, `price`, `price_compare`, `status`, `created_at`, `updated_at`, `featured_image`, `view`) VALUES
(30, '123', '123', '<p>123</p>', 123, 123, 123, 'deleted', '2018-09-23 01:27:05', '2018-09-23 01:27:05', '1537026700Screenshot%20from%202018-09-03%2000-17-03.png', 12),
(32, 'cac', 'cac', '<p>cac</p>', 123, 123, 123, 'deleted', '2018-09-18 04:01:21', '2018-09-18 04:01:21', '1537153145Screenshot%20from%202018-09-03%2000-17-03.png', 1),
(33, 'san pham test', 'test mo ta', '<p>test noi dung</p>', 1, 12038530, 12523935, 'deleted', '2018-09-23 01:27:05', '2018-09-23 01:27:05', '1537243367Screenshot.png', 14),
(34, 'Gấu bông', 'gấu bông man', '<p>l&agrave; con gấu b&ocirc;ng đ&oacute; man</p>', 2, 80000, 90000, 'active', '2018-09-25 03:07:09', '2018-09-25 03:07:09', '153766867514-600x800-260x350.jpg', 24);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Tuan Anh', 'email', 'password', NULL, NULL, NULL),
(2, 'Nguyen minh', 'thanhminh9p@gmail.com', '123456', NULL, NULL, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `collection`
--
ALTER TABLE `collection`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `collection_product`
--
ALTER TABLE `collection_product`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `meta`
--
ALTER TABLE `meta`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Chỉ mục cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Chỉ mục cho bảng `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `collection`
--
ALTER TABLE `collection`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `collection_product`
--
ALTER TABLE `collection_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT cho bảng `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `meta`
--
ALTER TABLE `meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT cho bảng `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
