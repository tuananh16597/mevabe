<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Coupon;

class CouponController extends Controller
{
  public static function checkStatus(Request $request, Response $response)
  {
    $code = $request->code;
    $cart = Helper::getCartFromSession();
    $coupon = Coupon::where('status', 'using')->where('code', $code)->get()->first();
    if (!empty($coupon)) {
      $message = "";
      if ($coupon->usage_left <= 0) {
        $message = "Usage <= 0";
        return response(['code' => -1, 'status' => 'fail', 'message'=>$message, 200])->header('Content-Type', 'text/plain');
      } else if ($coupon->end_date < date()) {
        $message = "Outdate";
        return response(['code' => -1, 'status' => 'fail', 'message'=>$message, 200])->header('Content-Type', 'text/plain');
      } else {
        return response(['code' => 0, 'status' => 'success', 'data' => $coupon, 200])->header('Content-Type', 'text/plain');
      }
    } else {
      return response(['code' => -1, 'status' => 'fail', 200])->header('Content-Type', 'text/plain');
    }
  }
}
