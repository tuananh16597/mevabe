<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
  public function getPage()
  {
    $id = $_GET['id'];
    $page = \App\Models\Page::where('status', 'active')->where('id', $id)->get()->first();
    if (empty($page)) {
      return view('content.404');

    }
    \App\Models\Page::updateView($id);
    return view('content.page', ['page' => $page]);
  }

}
