<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Customer;
use App\Models\Order;

class AdminCustomerController extends \App\Http\Controllers\Controller
{
  public function updateInfo(Request $request, Response $response)
  {
    if ($request->session()->has('admin')) {
      $data = (object)[];
      $data->name = $request->name;
      $data->email = $request->email;
      $data->phone = $request->phone;
      $data->address = $request->address;

      $data->id = $request->id;
      $customer_id = Customer::updateInfo($data);
      return response(['code'=>0, 'status'=>'success', 'result'=>$data], 200)->header('Content-Type', 'text/plain');
  } else {
      return response(['code' => -1, 'status' => 'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

  public function getCustomer(Request $request, Response $response) {
    if ($request->session()->has('admin')) {
      $id = Customer::where('id', $request->id)->get()->first();
      $orders = Order::join('customer', 'customer.id', 'order.customer_id')->where('customer_id', $id->id)
        ->select('customer.name', 'customer.phone', 'customer.email', 'customer.address', 'order.id', 'order.total','order.payment_method', 'order.order_status' , 'order.created_at')
        ->orderBy('id', 'desc')->get();
      return view('admin.customer_detail', ['current_page'=>'admin/customer', 'orders'=>$orders]);
    } else {
      return view('admin.login');
    }
  }
}