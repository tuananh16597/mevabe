<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Contact;

class AdminContactController extends \App\Http\Controllers\Controller
{
  public function getContacts(Request $request, Response $response) {
    if ($request->session()->has('admin')) {
      $contact_query = Contact::orderBy('created_at', 'desc');
      $page_name = "Tất cả";
      if (strpos($request->sort, 'only') !== false) {
        $contact_query = Contact::where('status', explode('-', $request->sort)[1])->orderBy('created_at', 'desc');
        $page_name = explode('-', $request->sort)[1];
      }
      $page = $request->page ? $request->page : 1;
      $perpage = $request->perpage ? $request->perpage : 8;
      $total_page = $contact_query->count() % $perpage > 0 ? intval($contact_query->count() / $perpage) + 1 : intval($contact_query->count() / $perpage);
      $total_item = $contact_query->count();
      $skip = ($page - 1) * $perpage;
      $contacts = $contact_query->skip($skip)->take($perpage)->get();
      $current_item = count($contacts);
      return view('admin.contact', [
        'current_page'=>'admin/contact_all',
        'contacts'=>$contacts,
        'page'=>$page,
        'perpage'=>$perpage,
        'total_page'=>$total_page,
        'current_items'=>$current_item,
        'total_item'=>$total_item,
        'page_name'=>$page_name
      ]);
    } else {
      return view('admin.login');
    }
  }

  public function getContact(Request $request) {
    if ($request->session()->has('admin')) {
      $contact = Contact::where('id', $request->id)->get()->first();
      return view('admin.contact_detail', ['current_page'=>'admin/contact_all', 'contact'=>$contact]);
    } else {
      return view('admin.login');
    }
  }

  public function changeStatusContact(Request $request) {
    if ($request->session()->has('admin')) {
      $ids = $request->ids;
      $status = $request->status;
      foreach ($ids as $id) {
        Contact::changeStatus($id, $status);
      }
      $sidebar_list = $request->session()->get('sidebar_list')->keyBy('href');
      $sidebar_contact = $sidebar_list->get('admin/contact_all');
      $sidebar_contact->counter = Contact::where('status', 'Unread')->count();
      $sidebar_list->push($sidebar_contact);
      return response(['code'=>0, 'status'=>'success'], 200)->header('Content-Type', 'text/plain');
    } else {
      return response(['code'=> -1, 'status'=>'fail'], 200)->header('Content-Type', 'text/plain');
    }
  }

}