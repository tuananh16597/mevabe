<?php
/**
 * Created by PhpStorm.
 * User: Hi
 * Date: 20/8/2018
 * Time: 4:52 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{

  public function getContact()
  {
    return view('content.contact');
  }

  public function contact(Request $request) {
    $contact = (object) [];
    $contact->name = $request->input("name");
    if (!$contact->name) {
      return view('content.contact', ['action_result'=>"Hãy nhập họ tên của bạn!"]);
    }
    $contact->email = $request->input("email");
    if(!filter_var($contact->email, FILTER_VALIDATE_EMAIL)) {
      return view('content.contact', ['action_result'=>"Email của bạn không hợp lệ!"]);
    }
    if (!$contact->email) {
      return view('content.contact', ['action_result'=>"Hãy nhập email của bạn!"]);
    }
    $contact->phone = $request->input("phone");
    if(!preg_match('/^\(?\+?([0-9]{1,4})\)?[-\. ]?(\d{3})[-\. ]?([0-9]{5,7})$/', trim($contact->phone))) {
      return view('content.contact', ['action_result'=>"Số điện thoại của bạn không hợp lệ!"]);
    }
    if (!$contact->phone) {
      return view('content.contact', ['action_result'=>"Hãy nhập số điện thoại của bạn!"]);
    }
    $contact->content = $request->input("content");
    if (!$contact->content) {
      return view('content.contact', ['action_result'=>"Hãy để lại lời nhắn cho chúng tôi!"]);
    }
    $contact = \App\Models\Contact::store($contact);

    return view('content.contact', ['action_result'=>"Chúng tôi đã nhận được liên hệ từ bạn!"]);
  }

}