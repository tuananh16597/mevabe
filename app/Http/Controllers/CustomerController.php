<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomerController extends Controller
{
  public function getLogin(Request $request)
  {
    if ($request->session()->get('login')) {
      return view('content.index');
    }
    return view('content.login');
  }

  public static function test(Request $request) {
    $orders = \App\Models\Order::where('customer_id', $request->id)->get();
    return $orders;
  }

  public function login(Request $request)
  {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $check_login = \App\Models\Customer::where('email', $email)->where('password', $password)->get()->first();
    if (!empty($check_login)) {
      $request->session()->put('login', true);
      $request->session()->put('customer', $check_login);
      if ($request->session()->has('cart') && count($request->session()->get('cart')->items) > 0) {
        return redirect('checkout');
      }
      return view('content.index');
    } else {
      return view('content.login', ['action_result' => 'Đăng nhập thất bại: Email hoặc Mật khẩu không đúng!']);
    }
  }

  public function logout(Request $request)
  {
    $request->session()->forget('login');
    $request->session()->forget('customer');
    $request->session()->forget('cart');
    return view('content.login');
  }

  public function getSignin(Request $request)
  {
    if ($request->session()->has('login') && $request->session()->get('login')) {
      return view('content.index');
    }
    return view('content.signin');
  }


  public function signin(Request $request)
  {
    $customer = (object)[];
    $customer->name = $request->input("name");
    if (!$customer->name) {
      return view('content.signin', ['action_result' => "Hãy nhập tên của bạn!"]);
    }
    $customer->email = $request->input("email");
    if (!$customer->email) {
      return view('content.signin', ['action_result' => "Hãy nhập email của bạn!"]);
    }
    //validation email
    $pattern_email = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i';
    if (!preg_match($pattern_email, $customer->email, $matches)) {
      return view('content.signin', ['action_result' => "Mời nhập đúng định dạng email!"]);
    }
    $customer->phone = $request->input("phone");
    //validation phone
    $pattern_phone = '/^[0-9]{10}+$/';
    if (!preg_match($pattern_phone, $customer->phone, $matches)) {
      return view('content.signin', ['action_result' => "Mời nhập đúng định dạng số điện thoại!"]);
    }
    if (!$customer->phone) {
      return view('content.signin', ['action_result' => "Hãy nhập số điện thoại của bạn!"]);
    }
    $customer->gender = $request->input("gender");
    $customer->address = $request->input("address");
    $customer->password = $request->input("password");
    $customer->confirm = $request->input("confirm");
    if (!$customer->password || $customer->password != $customer->confirm) {
      return view('content.signin', ['action_result' => "Mật khẩu của bạn không chính xác!"]);
    }
    //validation passwd
    $pattern_passwd = '/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,12}$/';
    if (!preg_match($pattern_passwd, $customer->password, $matches)) {
      return view('content.signin', ['action_result' => "Mật khẩu chứa ít nhất 8-12 ký tự, là chữ và là số!"]);
    }

    $customer = \App\Models\Customer::insert($customer);

    return view('content.signin_success');
  }

  //get view customer
  public function getCustomer(Request $request)
  {
    if ($request->session()->get('login'))
      return view('content.customer');
    else
      return view('content.login');
  }

  //get view change info
  public function getchangeInfo(Request $request)
  {
    $customer = $request->session()->get('customer');
    if ($request->session()->get('login'))
      return view('content.changeinfo', ['customer' => $customer]);
    else
      return view('content.login', ['action_result' => 'Người dùng chưa đăng nhập']);
  }

  //function change info
  public static function changeInfo(Request $request)
  {
    $customer = $request->session()->get('customer');
    $customer->name = $request->input("name");

    $customer->phone = $request->input("phone");
    //validation phone
    $pattern_phone = '/^[0-9]{10}+$/';
    if (!preg_match($pattern_phone, $customer->phone, $matches)) {
      return view('content.changeinfo', ['action_result' => "Mời nhập đúng định dạng số điện thoại!"]);
    }
    //validation email
    $customer->email = $request->input("email");
    $pattern_email = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i';
    if (!preg_match($pattern_email, $customer->email, $matches)) {
      return view('content.changeinfo', ['action_result' => "Mời nhập đúng định dạng email!"]);
    }

    $customer = \App\Models\Customer::change_info($customer);

    if ($request->session()->get('login'))
      return view('content.customer', ['customer' => $customer], ['action_result' => 'Thông tin tài khoản của bạn đã được cập nhật!']);
    else
      return view('content.login', ['action_result' => 'Người dùng chưa đăng nhập']);
  }

  //change passwd
  public static function getchangePasswd(Request $request)
  {
    $customer = $request->session()->get('customer');
    if ($request->session()->get('login'))
      return view('content.changepasswd', ['customer' => $customer]);
    else
      return view('content.login', ['action_result' => 'Người dùng chưa đăng nhập']);
  }

  //function change passwd
  public static function changePasswd(Request $request)
  {
    $customer = $request->session()->get('customer');

    $customer->password = $request->input("password");
    //validation passwd
    $pattern_passwd = '/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,12}$/';
    if (!preg_match($pattern_passwd, $customer->password, $matches)) {
      return view('content.changepasswd', ['action_result' => "Mật khẩu chứa ít nhất 8-12 ký tự, là chữ và là số!"]);
    }

    $customer->confirm = $request->input("confirm");

    $customer = \App\Models\Customer::change_password($customer);

//    if (!$customer->password || $customer->password != $customer->confirm) {
//      return view('content.changepasswd', ['action_result' => "Mật khẩu của bạn không chính xác!"]);
//    }

    if ($request->session()->get('login'))
      return view('content.customer', ['customer' => $customer], ['action_result' => 'Mật khẩu tài khoản của bạn đã được cập nhật!']);
    else
      return view('content.login', ['action_result' => 'Người dùng chưa đăng nhập']);
  }

  //view edit address
  public static function getchangeAddress(Request $request)
  {
    $customer = $request->session()->get('customer');
    if ($request->session()->get('login'))
      return view('content.changeaddress', ['customer' => $customer]);
    else
      return view('content.login', ['action_result' => 'Người dùng chưa đăng nhập']);
  }

  //view new address
  public static function changeaddress(Request $request)
  {
    if ($request->session()->get('login'))
      return view('content.newaddress');
    else
      return view('content.login', ['action_result' => 'Người dùng chưa đăng nhập']);
  }

  public static function newaddress(Request $request)
  {
    $customer = $request->session()->get('customer');

    if (!$customer->address) {
      return view('content.newaddress', ['action_result' => "Hãy nhập địa chỉ mới của bạn!"]);
    }
    $customer->address = $request->input("address");

    $customer = \App\Models\Customer::change_address($customer);

    if ($request->session()->get('login'))
      return view('content.customer', ['customer' => $customer], ['action_result' => 'Địa chỉ của bạn đã được cập nhật!']);
    else
      return view('content.login', ['action_result' => 'Người dùng chưa đăng nhập']);
  }

  public function getListOrder(Request $request) {
    $customer = $request->session()->get('customer');
    if ($request->session()->get('login'))
      return view('content.listorder', ['customer' => $customer]);
    else
      return view('content.login', ['action_result' => 'Người dùng chưa đăng nhập']);
  }
}
