<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
  protected $table = 'contact';

  public static function store($data)
  {
    $contact = new Contact;
    $contact->name = is_null($data->name) ? "" : $data->name;
    $contact->email = is_null($data->email) ? "" : $data->email;
    $contact->phone = is_null($data->phone) ? "" : $data->phone;
    $contact->content = is_null($data->content) ? "" : $data->content;
    $contact->status = "Unread";
    $contact->created_at = date('Y-m-d H:i:s');
    $contact->updated_at = date('Y-m-d H:i:s');

    $contact->save();
    return $contact->id;
  }

  public static function changeStatus($id, $status) {
    $contact = Contact::where('id', $id)->get()->first();
    $contact->updated_at = date('Y-m-d H:i:s');
    $contact->status = $status;
    $contact->save();
    return $contact->id;
  }
}