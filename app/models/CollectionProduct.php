<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollectionProduct extends Model
{
      protected $table = 'collection_product';

      public static function insertProduct($data) {
        $collection_product = new CollectionProduct;
        $collection_product->product_id = $data->product_id;
        $collection_product->collection_id = $data->collection_id;
        $collection_product->type = 'product';
        $collection_product->created_at = date('Y-m-d H:i:s');
        $collection_product->updated_at = date('Y-m-d H:i:s');
        $collection_product->save();
        return $collection_product->id;
      }

      public static function updateProduct($data) {
        CollectionProduct::where('type','product')->where('product_id', $data->product_id)->delete();
        foreach ($data->collection_ids as $collection_id) {
          $collection_product = new CollectionProduct;
          $collection_product->product_id = $data->product_id;
          $collection_product->collection_id = $collection_id;
          $collection_product->type = 'product';
          $collection_product->created_at = date('Y-m-d H:i:s');
          $collection_product->updated_at = date('Y-m-d H:i:s');
          $collection_product->save();
        }
      }

      public static function insertCollection($data) {
        $collection_product = new CollectionProduct;
        $collection_product->product_id = $data->product_id;
        $collection_product->collection_id = $data->collection_id;
        $collection_product->type = 'collection';
        $collection_product->created_at = date('Y-m-d H:i:s');
        $collection_product->updated_at = date('Y-m-d H:i:s');
        $collection_product->save();
        return $collection_product->id;
      }

      public static function updateCollection($data) {
        CollectionProduct::where('type','collection')->where('collection_id', $data->collection_id)->delete();
        foreach ($data->product_ids as $product_id) {
          $collection_product = new CollectionProduct;
          $collection_product->collection_id = $data->collection_id;
          $collection_product->product_id = $product_id;
          $collection_product->type = 'collection';
          $collection_product->created_at = date('Y-m-d H:i:s');
          $collection_product->updated_at = date('Y-m-d H:i:s');
          $collection_product->save();
        }
      }
}
