<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
  protected $table = 'customer';

  public static function insert($data)
  {
    $customer = new Customer;
    $customer->name = is_null($data->name) ? "" : $data->name;
    $customer->phone = is_null($data->phone) ? "" : $data->phone;
    $customer->gender = is_null($data->gender) ? "" : $data->gender;
    $customer->address = is_null($data->address) ? "" : $data->address;
    $customer->email = $data->email;
    $customer->password = $data->password;
    if ($customer->password) {
      $customer->password = password_hash($data->password, PASSWORD_DEFAULT);
    }
    $customer->created_at = date('Y-m-d H:i:s');
    $customer->updated_at = date('Y-m-d H:i:s');

    $customer->save();
    return $customer->id;
  }

  public static function change_info($data)
  {
    $customer = Customer::find($data->id);
    if (!$customer) return -2;
    $customer->name = $data->name;
    $customer->email = $data->email;
    $customer->phone = $data->phone ?: '';
    $customer->updated_at = date('Y-m-d H:i:s');
    $customer->save();
    return $customer;
  }

  public static function change_password($data)
  {
    $customer = Customer::find($data->id);
    if (!$customer) return -2;
    $customer->password = $data->password;
    if ($customer->password) {
      $customer->password = password_hash($data->password, PASSWORD_DEFAULT);
    }
    $customer->updated_at = date('Y-m-d H:i:s');
    $customer->save();
    return $customer;
  }

  public static function change_address($data) {
    $customer = Customer::find($data->id);
    if (!$customer) return -2;
    $customer->address = $data->address;
    $customer->updated_at = date('Y-m-d H:i:s');
    $customer->save();
    return $customer;
  }

  public static function updateCheckout($data)
  {
    $customer = \App\Models\Customer::where('id', $data->id)->get()->first();
    $customer->name = $data->name;
    $customer->email = $data->email;
    $customer->phone = $data->phone;
    $customer->address = $data->address;
    $customer->updated_at = date('Y-m-d H:i:s');
    $customer->update();
    return $customer->id;
  }

  public static function updateInfo($data)
  {
    $customer = \App\Models\Customer::where('id', $data->id)->get()->first();
    $customer->name = $data->name;
    $customer->email = $data->email;
    $customer->phone = $data->phone;
    $customer->address = $data->address;
    $customer->updated_at = date('Y-m-d H:i:s');
    $customer->update();
    return $customer->id;
  }

}
