<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $table = 'collection';

    public static function insert($data) {
        $collection = new Collection;
        $collection->title = $data->title;
        $collection->image = $data->image;
        $collection->description = $data->description;
        $collection->content = $data->content;
        $collection->created_at = date('Y-m-d H:i:s');
        $collection->updated_at = date('Y-m-d H:i:s');
        $collection->status = $data->status;
        $collection->view = isset($data->view) ? $data->view : 0;
        $collection->save();
        return $collection->id;
    }

    public static function updateView($id) {
      $collection = Collection::find($id);
      if ($collection) {
        $view = 1;
        if ($collection->view)
          $view = $collection->view + 1;
        $collection->view = $view;
        $collection->save();
      }
    }

    public static function update_collection($data) {
        $collection = Collection::find($data->id);
        $collection->title = $data->title;
        $collection->image = $data->image;
        $collection->description = $data->description;
        $collection->content = $data->content;
        $collection->updated_at = date('Y-m-d H:i:s');
        $collection->status = $data->status;
        $collection->save();
      return $collection->id;
    }

    public static function quickUpdate($data) {
        $collection = Collection::where('id', $data->id)->get()->first();
        $collection->title = $data->title;
        $collection->image = $data->image;
        $collection->updated_at = date('Y-m-d H:i:s');
        $collection->status = $data->status;
        $collection->save();
      return $collection->id;
    }

    public static function changeStatus($id, $status) {
      $collection = Collection::where('id', $id)->get()->first();
      $collection->updated_at = date('Y-m-d H:i:s');
      $collection->status = $status;
      $collection->save();
      return $collection->id;
    }
}
