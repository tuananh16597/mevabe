<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Coupon;
class Coupon extends Model
{
  protected $table = 'coupon';

  public static function setStatus($data) {
    $coupon = Coupon::where('status', 'using')->where('id', $data->id)->get()->first();
    $coupon->status = $data->status;

    $order->updated_at = date('Y-m-d H:i:s');
    $order->save();
  }

}
