<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $table = 'products';

  public static function insert($data) {
    $product = new Product;
    $product->title = $data->title;
    $product->featured_image = $data->featured_image;
    $product->description = $data->description;
    $product->content = $data->content;
    $product->status = $data->status;
    $product->price = $data->price;
    $product->price_compare = $data->price_compare;
    $product->stock_quant = $data->stock_manage;
    $product->view = isset($data->view) ? $data->view : 0;
    $product->created_at = date('Y-m-d H:i:s');
    $product->updated_at = date('Y-m-d H:i:s');
    $product->save();
    return $product->id;
  }

  public static function updateView($id) {
    $product = Product::find($id);
    if($product) {
      $view = 1;
      if($product->view) {
        $view = $product->view + 1;
      }
      $product->view = $view;
      $product->save();
      return $product->id;
    }
  }

  public static function update_product($data) {
    $product = Product::find($data->id);
    $product->title = $data->title;
    $product->featured_image= $data->image;
    $product->description = $data->description;
    $product->content = $data->content;
    $product->price = $data->price;
    $product->price_compare = $data->price_compare;
    $product->stock_quant = $data->stock_manage;
    $product->status = $data->status;
    $product->updated_at = date('Y-m-d H:i:s');
    $product->save();
    return $product->id;
  }

  public static function changeStatus($id, $status) {
    $product = Product::where('id', $id)->get()->first();
    $product->updated_at = date('Y-m-d H:i:s');
    $product->status = $status;
    $product->save();
    return $product->id;
  }
}
